package com.example.camporideapp.util;

public class Constants {
    public final static String PUBNUB_PUBLISH_KEY = "pub-c-4a88df8b-bbf7-4d24-8124-3fcdd571b45f";
    public final static String PUBNUB_SUBSCRIBE_KEY = "sub-c-4b455f84-a249-11e9-806d-8e5101bba1a5";
    public final static String PUBNUB_CHANNEL_NAME = "drivers_location";
}
